<?php

/**
 * @file
 * Admin form callbacks for AB Tasty module
 */

/**
 * AB Tasty settings form
 */
function ab_tasty_admin_form($form, &$form_state) {
  $form['ab_tasty_account_number'] = array(
    '#type' => 'textfield',
    '#title' => t('AB Tasty account number'),
    '#default_value' => variable_get('ab_tasty_account_number', NULL),
  );

  return system_settings_form($form);
}